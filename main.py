from flask import Flask, jsonify, render_template, request
import DataService as db
from Cafe import Cafe

app = Flask(__name__)
db.initialize_cafes_table()

@app.route("/")
def home():
    return render_template("index.html")

# # HTTP GET - Read Record
@app.route("/random")
def random():
    return jsonify(db.get_random_cafe())

@app.route("/all")
def all_cafes():
    return jsonify(db.get_all_cafes())

@app.route("/search")
def search_cafes_by_location():
    loc = request.args.get('loc')
    result = db.find_cafes_by_location(loc)
    if result:
        return jsonify(result)
    else:
        return {'error': 'Not Found, sorry bro'}

# # HTTP POST - Create Record
@app.route("/add", methods=['POST'])
def create_cafe():
    tmp = Cafe(
        id=0,
        name=request.form.get('name'),
        map_url=request.form.get('map_url'),
        img_url=request.form.get('img_url'),
        location=request.form.get('location'),
        seats=int(request.form.get('seats')),
        has_toilet=bool(request.form.get('has_toilet')),
        has_wifi=bool(request.form.get('has_wifi')),
        has_sockets=bool(request.form.get('has_sockets')),
        can_take_calls=bool(request.form.get('can_take_calls')),
        coffee_price=float(request.form.get('coffee_price')),

    )
    result = db.add_cafe(tmp)

    if result:
        return f'Created cafe (id={result})'
    else:
        return 'Something has failed u'

# # HTTP PUT/PATCH - Update Record
@app.route('/update-price/<int:cafe_id>', methods=['PATCH'])
def update_price_by_id(cafe_id):
    price = float(request.form.get('coffee_price'))
    if db.update_coffee_price_by_cafe_id(cafe_id, price):
        return {"success": "successfully updated the price"}, 200
    else:
        return {"error": "something went wrong, the cafe was not found in the database"}, 404

# # HTTP DELETE - Delete Record
@app.route('/report-closed/<int:cafe_id>', methods=['DELETE'])
def delete_cafe_by_id(cafe_id):
    if request.args.get('x-api-key') != 'TopSecretAPIKey':
        return {"error": "(correct) api-key is not present"}, 403
    if db.delete_cafe_by_id(cafe_id):
        return {"success": "cafe deleted"}
    else:
        return {"error": "cafe not found"}


if __name__ == '__main__':
    app.run(debug=True)
