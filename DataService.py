import sqlite3

from Cafe import Cafe


class Dataservice:
    DB_NAME = 'cafes.db'


def initialize_cafes_table() -> None:
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        cursor.execute("CREATE TABLE IF NOT EXISTS cafe ("
                       "id INTEGER PRIMARY KEY, "
                       "name varchar(250) NOT NULL UNIQUE, "
                       "map_url varchar(500) NOT NULL, "
                       "img_url varchar(500) NOT NULL, "
                       "location varchar(250) NOT NULL, "
                       "seats varchar(250) NOT NULL, "
                       "has_toilet BIT NOT NULL, "
                       "has_wifi BIT NOT NULL, "
                       "has_sockets BIT NOT NULL, "
                       "can_take_calls BIT NOT NULL, "
                       "coffee_price varchar(250)"
                       ")")


def get_all_cafes() -> list:
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        return [Cafe(id=tuple_[0],
                     name=tuple_[1],
                     map_url=tuple_[2],
                     img_url=tuple_[3],
                     location=tuple_[4],
                     seats=tuple_[5],
                     has_toilet=tuple_[6],
                     has_wifi=tuple_[7],
                     has_sockets=tuple_[8],
                     can_take_calls=tuple_[9],
                     coffee_price=tuple_[10])
                for tuple_ in cursor.execute('SELECT c.id, c.name, c.map_url, c.img_url, c.location, c.seats, '
                                             'c.has_toilet, c.has_wifi, c.has_sockets, c.can_take_calls, '
                                             'c.coffee_price '
                                             'FROM cafe AS c').fetchall()]


def get_random_cafe() -> Cafe:
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        sql = 'SELECT c.id, c.name, c.map_url, c.img_url, c.location, ' \
              'c.seats, c.has_toilet, c.has_wifi, c.has_sockets, ' \
              'c.can_take_calls, c.coffee_price ' \
              'FROM cafe as c ORDER BY RANDOM() LIMIT 1'
        tuple_ = cursor.execute(sql).fetchone()
        return Cafe(id=tuple_[0], name=tuple_[1], map_url=tuple_[2], img_url=tuple_[3], location=tuple_[4],
                    seats=tuple_[5], has_toilet=tuple_[6], has_wifi=tuple_[7], has_sockets=tuple_[8],
                    can_take_calls=tuple_[9], coffee_price=tuple_[10])


def find_cafes_by_location(location):
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM cafe AS c WHERE c.location = :location', {"location": location})
        list_of_tuples = cursor.fetchall()

        if len(list_of_tuples):
            list_of_cafes = [
                Cafe(id=tuple_[0], name=tuple_[1], map_url=tuple_[2], img_url=tuple_[3], location=tuple_[4],
                     seats=tuple_[5], has_toilet=tuple_[6], has_wifi=tuple_[7], has_sockets=tuple_[8],
                     can_take_calls=tuple_[9], coffee_price=tuple_[10]) for tuple_ in list_of_tuples]

            return list_of_cafes
        else:
            return None


def find_cafe_by_id(cafe_id):
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        result = cursor.execute('SELECT * FROM cafe WHERE id = ?', (cafe_id,)).fetchone()
        if result:
            return result
        else:
            return None


def add_cafe(c: Cafe):
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        cursor = connection.cursor()
        sql = 'INSERT INTO cafe (name, map_url, img_url, location, seats, has_toilet, has_wifi, ' \
              'has_sockets, can_take_calls, coffee_price) ' \
              'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
        params = (c.name, c.map_url, c.img_url, c.location, c.seats, int(c.has_toilet), int(c.has_wifi),
                  int(c.has_sockets), int(c.can_take_calls), c.coffee_price)
        cursor.execute(sql, params)
        connection.commit()
    return cursor.lastrowid


def update_coffee_price_by_cafe_id(cafe_id: int, price: float):
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        id_exists = find_cafe_by_id(cafe_id)
        if id_exists:
            cursor = connection.cursor()
            sql = 'UPDATE cafe SET coffee_price = ? WHERE id = ?'
            params = (price, cafe_id)
            cursor.execute(sql, params)
            connection.commit()
            return True
        else:
            return False


def delete_cafe_by_id(cafe_id):
    with sqlite3.connect(Dataservice.DB_NAME) as connection:
        id_exists = find_cafe_by_id(cafe_id)
        if id_exists:
            cursor = connection.cursor()
            sql = 'DELETE FROM cafe WHERE id=?'
            params = (cafe_id,)
            cursor.execute(sql, params)
            return True
        else:
            return False
