from dataclasses import dataclass


@dataclass
class Cafe:
    id: int
    name: str
    map_url: str
    img_url: str
    location: str
    seats: int
    has_toilet: bool
    has_wifi: bool
    has_sockets: bool
    can_take_calls: bool
    coffee_price: float
